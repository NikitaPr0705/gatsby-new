import React from "react";
import { Link, StaticQuery, graphql } from "gatsby";
import styled from "styled-components";

import { colors } from "../utils/vars";

const Sidebar = styled.section`
    position: fixed;
    left: 0;
    width: 20%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    background-color: ${colors.second};
    color: ${colors.textMain};
  `;

const navItem = `
    display: flex;
    align-items: center;
    margin: 0 1em 0 2em;
    padding: 0.5em 0;
    border-bottom: 0.05em solid ${colors.main50};
    position: relative;
    color: ${colors.textBody};
    text-decoration: none;

    &:last-child {
      border-bottom: none;
    }

    }
  `;

export default () => (
    <StaticQuery
        query={graphql`
        {
          allContentfulPost(sort: { order: ASC, fields: orderNumber }) {
            edges {
              node {
                title
                link
                orderNumber
              }
            }
          }
        }
      `}
        render={({ allContentfulPost: { edges } }) => (
            <Sidebar>
                {edges.map(({ node: { title, link, orderNumber } }) => (
                    <Link to={link} key={link} css={navItem}>
                        {orderNumber}. {title}
                    </Link>
                ))}
            </Sidebar>
        )}
    />
);
